import { Link } from "react-router-dom"
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { typography } from "@mui/system";

function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <typography variant="h6">
                
                ยินดีต้อนรับสู่เว็บคำนวณ BMI :
                
                </typography>
            <Link to="/">
                        <typography variant="body1">
                           เครื่องคิดเลข
                        </typography>
                </Link>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                <Link to="/about">
                        ผู้จัดทำ
                </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;

/*function Header() {
    return (

        <div className="header">
            <div className="temp-header">
                <span>
                    ยินดีต้อนรับสู่เว็บคำนวณ BMI:
                </span>
                <Link to="/">
                    <span>
                        เครื่องคิดเลข
                    </span>
                </Link>

                <Link to="/about">
                    <span>
                        ผู้จัดทำ
                    </span>
                </Link>
                <Link to="/Luckynumber">
                    <span>
                        สุ่มเลข
                    </span>
                </Link>
            </div>
            <hr />

        </div >

    );

}

export default Header; */