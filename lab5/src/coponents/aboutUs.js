import { Paper } from "@mui/material/Paper";
import { Box } from "@mui/material/Box";

function AboutUs (props) {

    
    return (
        <box sx={ {width:"60%"}}>
            <paper elevation={3}>
       <div>
           <h2> จัดทำโดย: { props.name}</h2>
           <h3> ติดต่อได้ทาง FB {props.address}</h3>
           <h3> อยู่ที่นครศรีธรรมราช {props.provinc}</h3>
       </div>
            </paper>
        </box>
    );

}

export default AboutUs;