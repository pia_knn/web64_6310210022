const mysql = require('mysql')

const connection = mysql.createConnection({
     host : 'localhost', 
     user: 'runner_admin', 
     password: 'runner_admin', 
     database: 'RunningSystem' 
    
})
connection.connect()

const express = require('express')
const app = express()
const port = 4000

/* CRUD Operation for RunningEvent Table */
app.get("/list_event", (req, res) => {
    query = "SELECT * from RunnerEvent"
    connection.query( query, (err, rows) => {
    if (err){
        console.log(err)
        res.json({
                    "status" : "400", 
                    "message" : "Error querying from running db"
                })
    }else{
        res.json(rows)
    }
    });
})


app.post("/add_event", (req, res) => {

      let event_name = req.query.event_name
      let event_location = req.query.event_location

      let query = `INSERT INTO RunnerEvent
                      (EventName, EventLocation)
                      VALUES ('${event_name}','${event_location}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            console.log(err)
            res.json({
                        "status" : "400", 
                        "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Adding event succesful"
            })
        }
    });

})


app.post("/update_event", (req, res) => {

    let event_id = req.query.event_id
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query = `UPDATE RunnerEvent SET
                    EventName='${event_name}',
                    EventLocation='${event_name}'
                    WHERE EventID=${event_id}`

  console.log(query)

  connection.query( query, (err, rows) => {
      if (err){
          console.log(err)
          res.json({
                      "status" : "400", 
                      "message" : "Error updating record"
                  })
      }else{
          res.json({
              "status" : "200", 
              "message" : "updating event succesful"
          })
      }
  });

})





app.listen(port, () => {
    console.log(` Now Starting Running System Backend ${port} `)

})

app.post("/add_event")

/*
query = "SELECT * from Runner"; 
connection.query( query, (err, rows) => { 
    if (err) { 
        console. log(err); 
    }else { 
        console. log(rows); 
    } 
}); 

connection.end(); */