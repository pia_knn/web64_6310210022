import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
function App() {
  return (
    <div className="App">
      <Header />
      <header className="App-header">
        <Body />
      </header>
      <Footer/>
      <Body/>

    </div>
  );
}

export default App;
