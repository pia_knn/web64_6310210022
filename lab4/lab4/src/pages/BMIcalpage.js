
import BMIResult from "../coponents/BMIResult";
import { useState } from "react";


function BMIcalPage() {

   const [ name, setName ] = useState("");
   const [ bmiResult, setBmiResult ] = useState("0");
   const [ translateResult, setTranslateResult ] = useState("");
   
   const [ heigth, setHeight] = useState("");
   const [ weight, setWeight] = useState("");

   function CalculateBMI(){
       let h = parseFloat(heigth);
       let w = parseFloat(weight);
       let bmi = w / (h * h);
       setBmiResult( w/ (h * h));
       if (bmiResult > 25) {
           setTranslateResult("ดีมาก"); 
       }else{
           setTranslateResult("เกือบดี");
       }
   }

 return (
     <div align="left">
         <div align="center">
             ยินดีต้อนรับสู่เว็บคำนวณ BMI
             <hr />

             คุณชื่อ:  <input type="text"
                            value={name} 
                            onChange={ (e) => {setName(e.target.value);} } /> <br />

             สูง:  <input type="text"
                       value={heigth} 
                       onChange={ (e) => {setHeight(e.target.value);} } /> <br />

             หนัก:  <input type="text" 
                        value={weight} 
                        onChange={ (e) => {setWeight(e.target.value);} } /> <br />
     

            <button onClick={(e) => {CalculateBMI()}}>Calculate</button>
            {    bmiResult != 0 &&
                <div>
                    <hr />
             นี่ผลการคำนวณ
  
             <BMIResult 
                  name={ name }
                  bmi = { bmiResult}
                  bmiResult = { translateResult }
                />
                </div>

            }
         </div>
     </div>
    )
 }

 export default BMIcalPage 